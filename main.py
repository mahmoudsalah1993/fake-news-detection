import sys
import pandas as pd
import csv
import json
import numpy
import tensorflow as tf
from datetime import datetime
tf.logging.set_verbosity(tf.logging.INFO)

buzzFeedPath = 'FakeNewsNet-master/FakeNewsNet-master/Data/BuzzFeed/'
politiFactPath = 'FakeNewsNet-master/FakeNewsNet-master/Data/PolitiFact/'
fakePath = 'FakeNewsContent'
realPath = 'RealNewsContent'
def load(gloveFile):
    return pd.read_table(gloveFile, sep=" ", index_col=0, header=None, quoting=csv.QUOTE_NONE)

def vec(words, w):
  try:
    return words.loc[w].as_matrix()
  except:
    return []

def genGram(example, n):
  words,dimensions = example.shape
  gram = numpy.array([])
  result = []
  for i in range(0, words-n):
    l = []
    for j in range(i, i+n):
      l.append(example[j])
    result.append(numpy.average(l,axis=0))
  gram = numpy.average(result,axis=0)
  return gram
  
def cnn_model_fn(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
  input_layer = tf.reshape(features["x"], [-1, 200, 4, 1])
  input_layer = input_layer
  # Convolutional Layer bigrams
  convBigrams = tf.layers.conv1d(
      inputs=input_layer[:,:,0,:],
      filters=32,
      kernel_size = [5],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  # Convolutional Layer trigrams
  convTrigrams = tf.layers.conv1d(
      inputs=input_layer[:,:,1,:],
      filters=32,
      kernel_size = [5],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  # Convolutional Layer quadgrams
  convQuadgrams = tf.layers.conv1d(
      inputs=input_layer[:,:,2,:],
      filters=32,
      kernel_size = [5],
      strides=1,
      padding="same",
      activation=tf.nn.relu)
      
  # Convolutional Layer pentgrams
  convPentgrams = tf.layers.conv1d(
      inputs=input_layer[:,:,3,:],
      filters=32,
      kernel_size = [5],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  #linear layers
  conv2Bigrams = tf.layers.conv1d(
      inputs=convBigrams,
      filters=32,
      kernel_size = [1],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  # Convolutional Layer trigrams
  conv2Trigrams = tf.layers.conv1d(
      inputs=convTrigrams,
      filters=32,
      kernel_size = [1],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  # Convolutional Layer quadgrams
  conv2Quadgrams = tf.layers.conv1d(
      inputs=convQuadgrams,
      filters=32,
      kernel_size = [1],
      strides=1,
      padding="same",
      activation=tf.nn.relu)
      
  # Convolutional Layer pentgrams
  conv2Pentgrams = tf.layers.conv1d(
      inputs=convPentgrams,
      filters=32,
      kernel_size = [1],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #1
  poolbi = tf.layers.max_pooling1d(inputs=conv2Bigrams, pool_size=[2], strides=2)
  pooltri = tf.layers.max_pooling1d(inputs=conv2Trigrams, pool_size=[2], strides=2)
  poolquad = tf.layers.max_pooling1d(inputs=conv2Quadgrams, pool_size=[2], strides=2)
  poolpent = tf.layers.max_pooling1d(inputs=conv2Pentgrams, pool_size=[2], strides=2)

  out1 = tf.add(poolbi, pooltri)
  out2 = tf.add(poolquad, poolpent)

  out = tf.add(out1, out2)
  relued = tf.nn.relu(out)
  prediction = tf.sigmoid(relued)
  # Dense Layer
  prediction_flat = tf.reshape(prediction, [-1, 100 * 32])
  dense = tf.layers.dense(inputs=prediction_flat, units=1024, activation=tf.nn.relu)
  dropout = tf.layers.dropout(
      inputs=dense, rate=0.9, training=mode == tf.estimator.ModeKeys.TRAIN)
  
  # Logits Layer
  logits = tf.layers.dense(inputs=dropout, units=2)

  predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }

  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

def main(argv):
  embeddings = load(argv[1])
  buzzfeedArticles = open(buzzFeedPath + 'News.txt', 'r').read().splitlines()
  politifactArticles = open(politiFactPath + 'News.txt', 'r').read().splitlines()
  trainingdata = []
  traininglabels = numpy.array([])
  evaldata = []
  evallabels = numpy.array([])
  i = 1
# load buzzfeed articles
  print("Loading buzzfeed")
  for article in buzzfeedArticles:
    label = article.split('_')[1]
    page = open(buzzFeedPath + '/' + label + 'NewsContent/' + article + '-Webpage.json' , 'r')
    text = json.load(page)['text']
    words = text.split(' ')
    entry = numpy.array([])

    for word in words:
      wordEmbedding = vec(embeddings, word)
      if wordEmbedding != []:
        numEmbedding = numpy.array(wordEmbedding)
        if len(entry) == 0:
          entry = numEmbedding
        else:
          entry = numpy.vstack((entry, numEmbedding))
    if entry.ndim > 1:
      bigram = genGram(entry, 2) 
      trigram = genGram(entry, 3)
      quadgram = genGram(entry, 4)
      pentgram = genGram(entry, 5)
      entry = numpy.concatenate([ bigram, trigram, quadgram, pentgram ])
      if i%9 ==0:
        evaldata.append(entry)
        evallabels = numpy.append(evallabels, 1 if label == 'Fake' else 0 )
      else:    
        trainingdata.append(entry)
        traininglabels = numpy.append(traininglabels, 1 if label == 'Fake' else 0 )
      i = i +1
      if i%20 == 0:
        print(i)

# load politiFact articles
  print("Loading politifact")
  i = 1
  for article in politifactArticles:
    label = article.split('_')[1]
    page = open(politiFactPath + '/' + label + 'NewsContent/' + article + '-Webpage.json' , 'r')
    text = json.load(page)['text']
    words = text.split(' ')
    entry = numpy.array([])

    for word in words:
      wordEmbedding = vec(embeddings, word)
      if wordEmbedding != []:
        numEmbedding = numpy.array(wordEmbedding)
        if len(entry) == 0:
          entry = numEmbedding
        else:
          entry = numpy.vstack((entry, numEmbedding))
    if entry.ndim > 1:
      bigram = genGram(entry, 2) 
      trigram = genGram(entry, 3)
      quadgram = genGram(entry, 4)
      pentgram = genGram(entry, 5)
      entry = numpy.concatenate([ bigram, trigram, quadgram, pentgram ])
      if i%9 ==0:
        evaldata.append(entry)
        evallabels = numpy.append(evallabels, 1 if label == 'Fake' else 0 )
      else:    
        trainingdata.append(entry)
        traininglabels = numpy.append(traininglabels, 1 if label == 'Fake' else 0 )
      i = i +1
      if i%20 == 0:
        print(i)

  # Create the Estimator
  fake_classifier = tf.estimator.Estimator(
    model_fn=cnn_model_fn, model_dir="/tmp/fake_cnn_model")
  
  # Set up logging for predictions
  tensors_to_log = {"probabilities": "softmax_tensor"}
  logging_hook = tf.train.LoggingTensorHook(
      tensors=tensors_to_log, every_n_iter=5000)
  #training section start
  print("Training start")
  print(datetime.now())
  # Train the model
  train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": numpy.array(trainingdata).astype(numpy.float32)},
    y=traininglabels.astype(numpy.int32),
    batch_size=100,
    num_epochs=None,
    shuffle=True)
  fake_classifier.train(
    input_fn=train_input_fn,
    steps=50000,
    hooks=[logging_hook])
  print("Training end")
  print(datetime.now())
  #training section end
  # Evaluate the model and print results
  print("Evaluation start")
  print(datetime.now())
  eval_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": numpy.array(evaldata).astype(numpy.float32)},
    y=evallabels.astype(numpy.int32),
    num_epochs=1,
    shuffle=False)
  eval_results = fake_classifier.evaluate(input_fn=eval_input_fn)
  print(eval_results)
  print("Evaluation end")
  print(datetime.now())

if __name__ == '__main__':
   main(sys.argv)