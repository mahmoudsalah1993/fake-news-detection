import sys
import pandas as pd
import csv
import json
import numpy
import tensorflow as tf
from datetime import datetime
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import NMF
import csv
import pickle
tf.logging.set_verbosity(tf.logging.INFO)

buzzFeedPath = 'FakeNewsNet-master/FakeNewsNet-master/Data/BuzzFeed/'
politiFactPath = 'FakeNewsNet-master/FakeNewsNet-master/Data/PolitiFact/'
fakePath = 'FakeNewsContent'
realPath = 'RealNewsContent'

def cnn_model_fn(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
  input_layer = tf.reshape(features["x"], [-1, 15,20])
  input_layer = input_layer
  # Convolutional Layer bigrams
  convlayer1 = tf.layers.conv1d(
      inputs=input_layer,
      filters=32,
      kernel_size = [5],
      strides=1,
      padding="same",
      activation=tf.nn.relu)

  #linear layers
  convlayer2 = tf.layers.conv1d(
      inputs=convlayer1,
      filters=32,
      kernel_size = [1],
      strides=1,
      padding="same",
      activation=tf.nn.relu)
 
  # Pooling Layer #1
  pooled = tf.layers.max_pooling1d(inputs=convlayer2, pool_size=[2], strides=2)
  relued = tf.nn.relu(pooled)
  prediction = tf.sigmoid(relued)
  # Dense Layer
  prediction_flat = tf.reshape(prediction, [-1, 7 * 32])
  dense = tf.layers.dense(inputs=prediction_flat, units=1024, activation=tf.nn.relu)
  dropout = tf.layers.dropout(
      inputs=dense, rate=0.9, training=mode == tf.estimator.ModeKeys.TRAIN)
  # Logits Layer
  logits = tf.layers.dense(inputs=dropout, units=2)
  predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }

  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

def saveEvalData():
    #eval data
    buzzfeedArticles = open(buzzFeedPath + 'News.txt', 'r').read().splitlines()
    politifactArticles = open(politiFactPath + 'News.txt', 'r').read().splitlines()
    evallabels = numpy.array([])
    evalarticles = []
    i = 1
    # load buzzfeed articles
    print("Loading buzzfeed")
    for article in buzzfeedArticles:
        label = article.split('_')[1]
        page = open(buzzFeedPath + '/' + label + 'NewsContent/' + article + '-Webpage.json' , 'r')
        text = json.load(page)['text']
        evalarticles.append(text)
        evallabels = numpy.append(evallabels, 1 if label == 'Fake' else 0 )
        i = i +1
        if i%20 == 0:
            print(i)

    # load politiFact articles
    print("Loading politifact")
    i = 1
    for article in politifactArticles:
        label = article.split('_')[1]
        page = open(politiFactPath + '/' + label + 'NewsContent/' + article + '-Webpage.json' , 'r')
        text = json.load(page)['text']
        evalarticles.append(text)
        evallabels = numpy.append(evallabels, 1 if label == 'Fake' else 0 )
        i = i +1
        if i%20 == 0:
            print(i)
    #news features
    vectorizer = CountVectorizer()
    evalnewsfeatures = vectorizer.fit_transform(evalarticles)
    evalmodel = NMF(n_components=300, init='random', random_state=0)
    Weval = evalmodel.fit_transform(evalnewsfeatures)
    Heval = evalmodel.components_
    # write a file
    f = open("eval.pickle", "wb")
    pickle.dump(Weval, f)
    pickle.dump(evallabels, f)
    f.close()

def loadData(picklename):
    f = open(picklename, "rb")
    a = pickle.load(f)
    b = pickle.load(f)
    f.close()
    return a,b

def saveTrainData():
  trainingarticles = []
  traininglabels = numpy.array([])
  with open('fake_or_real_news.csv', 'r') as f:
    reader = csv.reader(f)
    entries = list(reader)
    entries.pop(0)
    for entry in entries:
        trainingarticles.append(entry[1] + entry[2])
        traininglabels = numpy.append(traininglabels, 1 if entry[3] == 'FAKE' else 0 )
    #news features
    vectorizer = CountVectorizer()
    trainingnewsfeatures = vectorizer.fit_transform(trainingarticles) 
    trainmodel = NMF(n_components=300, init='random', random_state=0)
    Wtrain = trainmodel.fit_transform(trainingnewsfeatures)
    Htrain = trainmodel.components_
    f.close()
    # write a file
    f = open("train.pickle", "wb")
    pickle.dump(Wtrain, f)
    pickle.dump(traininglabels, f)
    f.close()

def main(argv):
#Create the Estimator
  if argv[1] == "save":
    saveEvalData()
    saveTrainData()
    exit(0)
  Wtrain, traininglabels = loadData("train.pickle")
  Weval, evallabels = loadData("eval.pickle")
  fake_classifier = tf.estimator.Estimator(
        model_fn=cnn_model_fn, model_dir="/tmp/fake_cnn_model")
  
  # Set up logging for predictions
  tensors_to_log = {"probabilities": "softmax_tensor"}
  logging_hook = tf.train.LoggingTensorHook(
      tensors=tensors_to_log, every_n_iter=5000)
  #training section start
  print("Training start")
  print(datetime.now())
  # Train the model
  train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": numpy.array(Wtrain).astype(numpy.float32)},
    y=traininglabels.astype(numpy.int32),
    batch_size=64,
    num_epochs=None,
    shuffle=True)
  fake_classifier.train(
    input_fn=train_input_fn,
    steps=100000,
    hooks=[logging_hook])
  print("Training end")
  print(datetime.now())
  #training section end
  # Evaluate the model and print results
  print("Evaluation start")
  print(datetime.now())
  eval_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": numpy.array(Weval).astype(numpy.float32)},
    y=evallabels.astype(numpy.int32),
    num_epochs=1,
    shuffle=False)
  eval_results = fake_classifier.evaluate(input_fn=eval_input_fn)
  print(eval_results)
  print("Evaluation end")
  print(datetime.now())

if __name__ == '__main__':
   main(sys.argv)